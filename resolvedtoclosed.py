#coding=utf-8

"""
This script uses bitbucket API to change all "resolved"
tickets in the specified repository to "closed"

Useful for those, who want to adapt their ticket lifecycle after
closed status was introduced on May, 13 in 2014.

Based on Atlassian's Bitbucket API Authentication example.

@author Alexey "datagreed" Strelkov
@email datagreed@gmail.com
"""


from rauth import OAuth1Service
 
def get_session():
    # Create a new consumer at https://bitbucket.org/account/user/{username}/api
    print "You can create new consumer key at https://bitbucket.org/account/user/{username}/api"
    CONSUMER_KEY = raw_input('Enter your consumer key here: ')
    CONSUMER_SECRET = raw_input('Enter your consumer secret here: ')
 
    # API URLs from https://confluence.atlassian.com/display/BITBUCKET/oauth+Endpoint
    REQUEST_TOKEN_URL = 'https://bitbucket.org/!api/1.0/oauth/request_token'
    ACCESS_TOKEN_URL = 'https://bitbucket.org/!api/1.0/oauth/access_token'
    AUTHORIZE_URL = 'https://bitbucket.org/!api/1.0/oauth/authenticate'
 
    # Create the service
    bitbucket = OAuth1Service(name='bitbucket',
                              consumer_key=CONSUMER_KEY,
                              consumer_secret=CONSUMER_SECRET,
                              request_token_url=REQUEST_TOKEN_URL,
                              access_token_url=ACCESS_TOKEN_URL,
                              authorize_url=AUTHORIZE_URL)
 
 
    # Change CALL_BACK to something that listens for a callback
    # with oauth_verifier in the URL params and automatically stores
    # the verifier.
    CALL_BACK = 'http://localhost?dump'
 
    # Make the request for a token, include the callback URL.
    rtoken, rtoken_secret = bitbucket.get_request_token(params={'oauth_callback': CALL_BACK})
 
    # Use the token to rquest an authorization URL.
    authorize_url = bitbucket.get_authorize_url(rtoken)
 
    # Send the user to Bitbucket to authenticate. The CALL_BACK is the
    # URL. The URL is redirected to after success. Normally, your
    # application automates this whole exchange.
    print 'Visit %s in new browser window.' % (authorize_url)
 
    # You application should also automated this rather than request
    # it from the user.    
    print "After you visited the page, copy oauth_verifier (usually 10 digits) from the URL"     
    oauth_verifier = raw_input('Enter oauth_verifier here: ')
 
    # Returns a session to Bitbucket using the verifier from above.
    return bitbucket.get_auth_session(rtoken, rtoken_secret, data={'oauth_verifier': oauth_verifier})
 
 
if __name__ == '__main__':
    session = get_session()
 
    username = raw_input('Enter username for repository here: ')
    repo_slug = raw_input('Enter repository name here: ')
    filter_status = "resolved"
    new_status = "closed"

    total_resolved = 999999
    step = 50
    issue_ids = []

    url = 'https://bitbucket.org/api/1.0/repositories/%s/%s/issues/' % (username, repo_slug)
    
    offset = 0
    limit = step

    print "Fetching {} tickets ids...".format("filter_status")

    while offset<total_resolved:

        params = {"limit":limit, "start":offset, "status":filter_status}

        resp = session.get(url,params=params)
        print url, params
        print resp
        #print resp.json() 
        
        js = resp.json()

        total_resolved = int(js['count'])

        for issue in js['issues']:

            issue_ids.append(issue['local_id'])
        
        offset+=step

    print "Saved {} issue ids".format(len(issue_ids))
    print "Total issues checksum: ", total_resolved
    #print issue_ids

    proceed = raw_input('Change all above tickets to {}?(y/n): '.format(new_status))

    if proceed.lower()=='y':

        for issue_id in issue_ids:

            url = 'https://bitbucket.org/api/1.0/repositories/%s/%s/issues/%s' % (username, repo_slug, issue_id)
            data = {'status': new_status}

            print url, data

            resp = session.put(url,data=data)

            print resp

        print "Done"
    else:
        print "Canceled"

   