This script uses bitbucket API to change all "resolved"
tickets in the specified repository to "closed"

Useful for those, who want to adapt their ticket lifecycle after
closed status was introduced on May, 13 in 2014.

Based on Atlassian's Bitbucket API Authentication example.